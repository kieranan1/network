﻿using Ether.Network.Client;
using Ether.Network.Packets;
using System;
using System.Net.Sockets;

namespace Client
{
	internal sealed class Client : NetClient
	{

		private bool Connected = false;

		/// <summary>
		/// Creates a new <see cref="MyClient"/>.
		/// </summary>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <param name="bufferSize"></param>
		public Client(string host, int port, int bufferSize)
		{
			this.Configuration.Host = host;
			this.Configuration.Port = port;
			this.Configuration.BufferSize = bufferSize;
		}

		/// <summary>
		/// Handles incoming messages.
		/// </summary>
		/// <param name="packet"></param>
		public override void HandleMessage(NetPacket packet)
		{
			Console.WriteLine("Incomming packet " + packet.PacketID);
		}

		/// <summary>
		/// Triggered when connected to the server.
		/// </summary>
		protected override void OnConnected()
		{
			this.Connected = true;
			Console.WriteLine("Connected to server");
		}

		/// <summary>
		/// Triggered when disconnected from the server.
		/// </summary>
		protected override void OnDisconnected()
		{
			if (this.Connected)
			{
				Console.WriteLine("Disconnected from server");
			}
		}

		/// <summary>
		/// Triggered when an error occurs.
		/// </summary>
		/// <param name="socketError"></param>
		protected override void OnSocketError(SocketError socketError)
		{
			Console.WriteLine("Socket Error: {0}", socketError.ToString());
		}
	}
}