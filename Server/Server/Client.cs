﻿using System;
using System.Collections.Generic;
using System.Text;

using Ether.Network.Common;
using Ether.Network.Packets;

namespace Server
{
	internal sealed class Client : NetUser
	{
		/// <summary>
		/// Send hello to the incoming clients.
		/// </summary>
		public void SendFirstPacket()
		{
			using (var packet = new NetPacket(1))
			{
				packet.Write("Hi");

				this.Send(packet);
			}
		}

		/// <summary>
		/// Receive messages from the client.
		/// </summary>
		/// <param name="packet"></param>
		public override void HandleMessage(NetPacket packet)
		{
			Console.WriteLine("Packet: " + packet.PacketID);
		}
	}

}
